/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native';
import NavigationExperimental from 'react-native-deprecated-custom-components';

import TaskInfo from './src/taskInfo';
import Tasks from './src/tasks';

var ROUTES = {
    Tasks: Tasks,
    TaskInfo: TaskInfo
};

export default class App extends Component<{}> {

    renderScene(route, navigator) {
        if(route.name == 'Tasks') {
            return <Tasks navigator={navigator}
                /*onBack={
                    
                }*/
            />
        }
        if(route.name == 'TaskInfo') {
            return <TaskInfo navigator={navigator} value={route.value}/>
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationExperimental.Navigator
                    initialRoute={{'name': 'Tasks', value:''}}
                    renderScene={this.renderScene}
                    configureScene={ () => { return NavigationExperimental.Navigator.SceneConfigs.FloatFromRight; } }
                />
            </View>
        );
    }

}

  const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  });

