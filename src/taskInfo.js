/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight
} from 'react-native';

import { Container, Header, Left, Right} from 'native-base';
import {Select, Option} from "react-native-chooser";
import DatePicker from 'react-native-datepicker'
import Button from 'react-native-button';
import Storage from './storage';
import Styles from './Style';
//const instructions = Platform.select({
//  ios: 'Press Cmd+R to reload,\n' +
//    'Cmd+D or shake for dev menu',
//  android: 'Double tap R on your keyboard to reload,\n' +
//    'Shake or press menu button for dev menu',
//});

export default class TaskInfo extends Component<{}> {

    constructor(props) {
        super(props);
        var date = new Date();
        if (this.props.value.key != null){
            this.state = { 
                key:this.props.value.key,
                name: this.props.value.value.name,
                description: this.props.value.value.description,
                importance: this.props.value.value.importance,
                dateTo: this.props.value.value.dateTo,
                dateEnd: this.props.value.value.dateEnd,
                curDate: date.getDate()+"-"+(+date.getMonth()+1)+"-"+date.getFullYear()
            };
        }
        else{
            this.state = { 
                key:'',
                name: '',
                description: '',
                importance: 'Обычная',
                dateTo: '',
                dateEnd: 'Нет',
                curDate: date.getDate()+"-"+(+date.getMonth()+1)+"-"+date.getFullYear()
            };
        }
    }


    /*https://gist.github.com/LeverOne/1308368 UUID*/
    generateKey(a, b){
        for(b=a='';a++<36;b+=a*51&52?(a^15?8^Math.random()*(a^20?16:4):4).toString(16):'-');
        return b
    }
    save(){
        if(this.props.value.key != null) 
            Storage.updateItem(this.props.value.key, this.state);
        else 
            Storage.setItem(this.generateKey(0, (Math.random()*10).toFixed()), this.state);
        this.props.navigator.pop();
    }

    
    render() {
        return (
            <View>
                <Header>
                    <Left>
                        <Button
                            style={{fontSize: 20, color: 'white', marginTop: 10}}
                            styleDisabled={{color: 'red'}}
                            onPress={() => this.props.navigator.pop()}>
                            Назад
                        </Button>
                    </Left>
                    <Right>
                        <Button
                            style={{fontSize: 20, color: 'white', marginTop: 10, marginLeft:10}}
                            styleDisabled={{color: 'red'}}
                            onPress={() => this.save()}>
                            Сохранить
                        </Button>
                    </Right>
                </Header>
                <View style={{margin: 15}}>           
                    
                    <TextInput
                        style={Styles.inputName}
                        onChangeText={(name) => this.setState({name})}
                        value={this.state.name}
                        placeholder="Название"
                    />
                    <TextInput
                        multiline = {true}
                        numberOfLines = {4}
                        onChangeText={(description) => this.setState({description})}
                        value={this.state.description}
                        placeholder="Описание"
                    />
                    <View style={Styles.flowRight}>
                        <Text>Важность задачи:</Text>
                        <Select
                            onSelect = {(importance) => this.setState({importance})}
                            defaultText  = {this.state.importance}
                            textStyle = {{}}
                            backdropStyle  = {{backgroundColor : "#d3d5d6"}}
                            optionListStyle = {{backgroundColor : "#F5FCFF"}}
                            style={{marginLeft:10}}
                        >
                            <Option value = "Обычная">Обычная</Option>
                            <Option value = "Важная">Важная</Option>
                            <Option value = "Очень важная">Очень важная</Option>
                        </Select>
                    </View>  
                    <View style={Styles.flowRight}>
                        <Text>Выполнить до:</Text>
                        <DatePicker
                            style={Styles.datePicker}
                            date={this.state.dateTo}
                            mode="date"
                            placeholder="Выберите дату"
                            format="DD-MM-YYYY"
                            minDate={this.state.curDate}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(dateTo) => {this.setState({dateTo: dateTo})}}
                        />
                    </View>  
                    
                    <Text>Задача выполнена: {this.state.dateEnd}</Text>
                
                </View>
            </View>
        );
    }
}

