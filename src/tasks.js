import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  ListView,
  TouchableHighlight,
  Navigator, CheckBox
} from 'react-native';
import Button from 'react-native-button';
import { Container, Header, Content, Card, CardItem, Body } from 'native-base';
import TaskInfo from './taskInfo';
import Storage from './storage';
import Styles from './Style';

export default class Tasks extends Component<{}> {

	constructor(props) {
	    super(props);
	    this.state = {
	    	dataSource: []
	    };
	    
	    this.loadData();
	}

	loadData(){
		Storage.getAsyncStorage().getAllKeys(async (err, keys) => {
		  	await Storage.getAsyncStorage().multiGet(keys, (err, stores) => {
		  		var tasks = [];
			   	stores.map((result, i, store) => {
			   		tasks.push(
			   			{
			   				key: store[i][0],
			   				value: JSON.parse(store[i][1])
			   			}
			   		);
			    }); 
			    this.setState({dataSource: tasks});
			});
		});
	}

	navigate(routeName, value){
		this.props.navigator.push({
			name: routeName,
			value:value
		});
	}

	render() {
		return (
			<Container>
				<Header>
		        	<Button
			            style={{fontSize: 20, color: 'white', marginTop: 10}}
			            styleDisabled={{color: 'gray'}}
			            onPress={this.navigate.bind(this, "TaskInfo")}>
			            Новая задача
			        </Button>
		        </Header>

		        <Content>
		        	{this.state.dataSource.map((value, key) => {  //alert("key = " + value.key + " value = "+JSON.stringify(value.value, "", 4));
		        		return (
			        		<TouchableHighlight onPress={this.navigate.bind(this, "TaskInfo", value, false)}>
						        <Card style={Styles.task}>
							        	<CheckBox checked={true}
							        			onChange={(checked) => {
							        				//alert(" value = "+Object.keys(checked));
							        				Storage.removeItem(value.key);
							        				this.loadData();
							        			}}
							        	/>
							            <Text style={Styles.title}>{value.value.name}</Text>
						        </Card>
					        </TouchableHighlight>
			        	);
			        })}
		        </Content>
		        
		        
			</Container>
		);
	}
	

}