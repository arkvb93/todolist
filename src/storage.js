import { AsyncStorage } from 'react-native';

const Storage = Object.assign({}, AsyncStorage, {
	getItem(key) {
		return  AsyncStorage.getItem(key).then((res) => JSON.parse(res));
	},

	getAsyncStorage(){
		return AsyncStorage;
	},

	getItems(){
		var tasks = [];
		AsyncStorage.getAllKeys(async (err, keys) => {
		  	await AsyncStorage.multiGet(keys, (err, stores) => {
			   	stores.map((result, i, store) => {
			   		tasks.push(
			   			{
			   				key: store[i][0],
			   				value: JSON.parse(store[i][1])
			   			}
			   		);
			    });
			});
		});
		return tasks;
	},

	updateItem(key, value) {
		return AsyncStorage.mergeItem(key, JSON.stringify(value));
	},

	setItem(key, value) {
		return AsyncStorage.setItem(key, JSON.stringify(value));
	},

	removeItem(key) {
		return AsyncStorage.removeItem(key);
	}
	
});

export default Storage;