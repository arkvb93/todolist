import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
	container: {
    	flex: 1,
    	justifyContent: 'flex-start',
    	/*alignItems: 'center',*/
    	backgroundColor: '#F5FCFF',
    	margin: 15,
  	},
  	inputName: {
  	  	fontSize: 20,
  	},
  	instructions: {
  	  	textAlign: 'center',
  	  	color: '#333333',
  	  	marginBottom: 5,
  	},
  	textInput: {
  	  	flex: 1
  	},
  	flowRight: {
  	  	flexDirection: 'row',
  	  	alignItems: 'center',
  	  	alignSelf: 'stretch'
  	},
  	datePicker: {
  	  	width: 200,
  	  	marginTop: 10,
  	  	marginLeft: 30
  	},
  	title: {
	    fontSize: 20,
	    textAlign: 'center',
	    margin: 20,
	  },
	task: {
	  	flex: 1,
	    justifyContent: 'center',    
	    /*alignItems: 'center',*/
	    backgroundColor: '#F5FCFF',
	    /*backgroundColor: '#E9967A',*/
	    height: 50,
	    marginLeft: 10,
	    marginRight: 10,
	    marginTop: 3,
	    marginBottom: 3,
	    /*borderRadius: 10*/
	},
});
export default Styles;